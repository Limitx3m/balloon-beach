﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public GameObject sceneManager;
    public float playerSpeed = 1500;
    public float directionalSpeed = 20;
    public AudioClip scoreUp;
    public AudioClip damage;
    public static bool canMove = false;

    // Update is called once per frame
    void Update()
    {
        playerSpeed += 0.5f;
        if(canMove)
        {
            HandleMovement();
        }
        
    }

    private void HandleMovement()
    {
        float deltaTime = Time.deltaTime;

#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_WEBPLAYER
        float moveHorizontal = Input.GetAxis("Horizontal");
        Vector3 currentPosition = gameObject.transform.position;
        Vector3 newPosition = new Vector3(Mathf.Clamp(currentPosition.x + moveHorizontal, -2.5f, 2.5f), currentPosition.y, currentPosition.z);

        transform.position = Vector3.Lerp(currentPosition, newPosition, directionalSpeed * deltaTime);
#endif            
        GetComponent<Rigidbody>().velocity = Vector3.forward * playerSpeed * deltaTime;
        transform.Rotate(Vector3.right * GetComponent<Rigidbody>().velocity.z / 3);

        // MOBILE CONTROLS
        Vector2 touch = Camera.main.ScreenToWorldPoint(Input.mousePosition + new Vector3(0, 0, 10f));
        if (Input.touchCount > 0)
        {
            transform.position = new Vector3(touch.x, transform.position.y, transform.position.z);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "scoreup")
        {
            GetComponent<AudioSource>().PlayOneShot(scoreUp, 1.0f);
        }
        if (other.gameObject.tag == "triangle")
        {
            GetComponent<AudioSource>().PlayOneShot(damage, 1.0f);
            sceneManager.GetComponent<App_Initalize>().GameOver();
        }
    }
}
